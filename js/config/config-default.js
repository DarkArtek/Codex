var config = {
    // GENERAL
    timeout: 5000,
    refresh: 100,
    // GAUGES
    edges: "round",
    zoom: 1,
    fontSize: 15,
    disabled: {},
    color: {},
    order: {},
    danger: false,
    glow: true,
    glows: {},
    // BUFFS
    all_personals: false,
    own_personals: true,
    buffs_disabled: {},

};